## Magnetic fields of solenoids and magnets
###Overview##
These [MATLAB functions](https://fr.mathworks.com/matlabcentral/fileexchange/71881-magnetic-fields-of-solenoids-and-magnets) perform analytical calculations of the magnetic field of finite solenoids (which is also the field of cylindrical magnets) and finite cuboidal magnets

###Description##
* The file 'FieldBar.m' calculates the cuboidal magnet field with [Camacho & Sosa, Rev. Mex. E, E 59, 8-17, 2013](http://www.scielo.org.mx/pdf/rmfe/v59n1/v59n1a2.pdf)
* The file 'FieldSolenoid.m' calculates the cylindrical magnet (=solenoid) field with (corrected) formula of [Callaghan & Maslen (1960)](https://archive.org/details/nasa_techdoc_19980227402) and [Derby & Olbert, Am. J. Phys. 78, 229 , 2010](https://doi.org/10.1119/1.3256157)
* The file 'PlotField.m' plot the magnetic field in various cases, for cylindrical magnets (=solenoids) and cuboidal magnets.

WARNING/REQUIREMENT: the [included files](https://fr.mathworks.com/matlabcentral/fileexchange/51989-maple-elliptic-integrals) whose name start with 'Elliptic' calculate the needed elliptic integrals by relying on [Elliptic_Integrals.zip](https://fr.mathworks.com/matlabcentral/fileexchange/3705-elliptic_integrals-zip) by Thomas Hoffend, which has thus be downloaded.

Alternatively, ['Elliptic Integrals'](https://fr.mathworks.com/matlabcentral/fileexchange/8805-elliptic-integrals-and-functions?s_tid=srchtitle), by Moiseev Igor, can be used by commenting/uncommenting 2 lines in the files whose name start with 'Elliptic'.

